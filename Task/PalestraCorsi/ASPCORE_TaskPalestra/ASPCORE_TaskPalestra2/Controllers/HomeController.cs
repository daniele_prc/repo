﻿using ASPCORE_TaskPalestra2.Data;
using ASPCORE_TaskPalestra2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TaskPalestra2.Controllers
{
    public class HomeController : Controller
    {
        private readonly InterfaceRepo<Corso> _repositoryCor;
        private readonly InterfaceRepo<Utente> _repositoryUte;
        private readonly InterfaceRepo<Iscrizione> _repositoryIsc;

        public HomeController(InterfaceRepo<Corso> repCor, InterfaceRepo<Utente> repUte, InterfaceRepo<Iscrizione> repIsc)
        {
            _repositoryCor = repCor;
            _repositoryUte = repUte;
            _repositoryIsc = repIsc;
        }

        [HttpGet]
        public IActionResult Index()
        {
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            List<Corso> elenco = (List<Corso>)_repositoryCor.GetAll();

            ViewBag.Title = "Lista Corsi - Palestra";

            return View(elenco);
        }

        // REGISTER------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult Registrazione()
        {
            ViewBag.Title = "Registrazione Utente - Palestra";

            return View(new Utente());
        }

        [HttpPost]
        public IActionResult Registrazione(Utente objUtente)
        {

            if (ModelState.IsValid)
            {
                _repositoryUte.Insert(objUtente);
                return Redirect("/");
            }
            else
            {

                return View(objUtente);
            }

        }

        // LOGIN------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult Accesso()
        {
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            ViewBag.Title = "Accesso Utente - Palestra";

            return View();
        }

        [HttpPost]
        public RedirectResult LoginCheck(Utente objUtente)
        {

            Utente login = _repositoryUte.GetByEmail(objUtente.Email);
            if (login != null && login.Password == objUtente.Password)
            {
                HttpContext.Session.SetString("isLogged", login.Email);

                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/Accesso");
            }
        }

        // ------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult IscrizioneCorso([FromRoute] int id)
        {
            
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            if (email == null)
            {
                return Redirect("/Home/Accesso");
            }

            ViewBag.User = true;

            Corso corso = _repositoryCor.GetById(id);
            Utente utente = _repositoryUte.GetByEmail(email);



            if (corso != null && utente != null)
            {
                Iscrizione iscrizione = new Iscrizione()
                {
                    CorsoRif = corso.CorsoId,
                    UtenteRif = utente.UtenteId,

                    CorsoRifNavigation = corso,
                    UtenteRifNavigation = utente
                };

                utente.Iscrizioni.Add(iscrizione);
                corso.Iscrizioni.Add(iscrizione);

                _repositoryIsc.Insert(iscrizione);

                return Redirect("/Home/Index");
            }

            return Ok("Errore");
        }

        // ------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult CorsiUtente()
        {
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            if (email == null)
                return Redirect("/Home/Accesso");
            else
            {
                Utente user = _repositoryUte.GetByEmail(email);

                List<Iscrizione> lista = _repositoryIsc.GetAll().Where(i => i.UtenteRif == user.UtenteId).ToList();
                foreach (Iscrizione item in lista)
                {
                    item.UtenteRifNavigation = _repositoryUte.GetById((int)item.UtenteRif);
                    item.CorsoRifNavigation = _repositoryCor.GetById((int)item.CorsoRif);
                }
                return View(lista);
            } 
        }

        // DELETE------------------------------------------------------------------------------------------

        [HttpGet]
        public IActionResult EliminaIscrizione(int id)
        {
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            if (_repositoryIsc.Delete(id))
                return Redirect("/Home/CorsiUtente");

            else
                return Ok("Errore nell'eliminazione");
        }

        // ------------------------------------------------------------------------------------------

        public IActionResult Logout()
        {
            string email = HttpContext.Session.GetString("isLogged");
            ViewData["User"] = email;

            HttpContext.Session.Clear();

            return Redirect("/Home/Index");

        }
    }
}
