﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ASPCORE_TaskPalestra2.Models
{
    public partial class Utente
    {
        public Utente()
        {
            Iscrizioni = new HashSet<Iscrizione>();
        }

        public int UtenteId { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Indirizzo { get; set; }
        public DateTime DataNascita { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Iscrizione> Iscrizioni { get; set; }
    }
}
