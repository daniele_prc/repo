﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ASPCORE_TaskPalestra2.Models
{
    public partial class Iscrizione
    {
        public int IscrizioneId { get; set; }
        public int CorsoRif { get; set; }
        public int UtenteRif { get; set; }

        public virtual Corso CorsoRifNavigation { get; set; }
        public virtual Utente UtenteRifNavigation { get; set; }

        //public static implicit operator Iscrizione(Iscrizione v)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
