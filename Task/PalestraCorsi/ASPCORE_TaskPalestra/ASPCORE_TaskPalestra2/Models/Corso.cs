﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ASPCORE_TaskPalestra2.Models
{
    public partial class Corso
    {
        public Corso()
        {
            Iscrizioni = new HashSet<Iscrizione>();
        }

        public int CorsoId { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public DateTime Orario { get; set; }

        public virtual ICollection<Iscrizione> Iscrizioni { get; set; }
    }
}
