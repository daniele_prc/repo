﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ASPCORE_TaskPalestra2.Models
{
    public partial class db_palestra2Context : DbContext
    {
        public db_palestra2Context()
        {
        }

        public db_palestra2Context(DbContextOptions<db_palestra2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Corso> Corsi { get; set; }
        public virtual DbSet<Iscrizione> Iscrizioni { get; set; }
        public virtual DbSet<Utente> Utenti { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-5MTRE45\\SQLEXPRESS;Database=db_palestra2;User Id=sharpuser;Password=pincopallino;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Corso>(entity =>
            {
                entity.ToTable("Corso");

                entity.Property(e => e.CorsoId).HasColumnName("corsoID");

                entity.Property(e => e.Descrizione)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("descrizione");

                entity.Property(e => e.Orario)
                    .HasColumnType("datetime")
                    .HasColumnName("orario");

                entity.Property(e => e.Titolo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("titolo");
            });

            modelBuilder.Entity<Iscrizione>(entity =>
            {
                entity.ToTable("Iscrizione");

                entity.Property(e => e.IscrizioneId).HasColumnName("iscrizioneID");

                entity.Property(e => e.CorsoRif).HasColumnName("corsoRIF");

                entity.Property(e => e.UtenteRif).HasColumnName("utenteRIF");

                entity.HasOne(d => d.CorsoRifNavigation)
                    .WithMany(p => p.Iscrizioni)
                    .HasForeignKey(d => d.CorsoRif)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Iscrizion__corso__29572725");

                entity.HasOne(d => d.UtenteRifNavigation)
                    .WithMany(p => p.Iscrizioni)
                    .HasForeignKey(d => d.UtenteRif)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Iscrizion__utent__2A4B4B5E");
            });

            modelBuilder.Entity<Utente>(entity =>
            {
                entity.ToTable("Utente");

                entity.HasIndex(e => e.Email, "UQ__Utente__AB6E6164D3156E17")
                    .IsUnique();

                entity.Property(e => e.UtenteId).HasColumnName("utenteID");

                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("cognome");

                entity.Property(e => e.DataNascita)
                    .HasColumnType("date")
                    .HasColumnName("dataNascita");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Indirizzo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("indirizzo");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nome");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("password");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
