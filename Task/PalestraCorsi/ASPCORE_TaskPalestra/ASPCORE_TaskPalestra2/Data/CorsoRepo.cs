﻿using ASPCORE_TaskPalestra2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TaskPalestra2.Data
{
    public class CorsoRepo : InterfaceRepo<Corso>
    {
        private readonly db_palestra2Context _context;

        public CorsoRepo(db_palestra2Context con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Corso> GetAll()
        {
            return _context.Corsi.ToList();
        }

        public Corso GetByEmail(string varEmail)
        {
            throw new NotImplementedException();
        }

        public Corso GetById(int varId)
        {
            return _context.Corsi.Where(c => c.CorsoId == varId).FirstOrDefault();
        }

        public Corso Insert(Corso t)
        {
            _context.Corsi.Add(t);
            _context.SaveChanges();

            return t;
        }

        public bool Update(Corso t)
        {
            throw new NotImplementedException();
        }
    }
}
