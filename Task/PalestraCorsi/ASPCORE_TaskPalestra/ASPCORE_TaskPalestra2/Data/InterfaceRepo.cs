﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TaskPalestra2.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
        T Insert(T t);
        bool Update(T t);
        bool Delete(int varId);
        T GetByEmail(string varEmail);
    }
}
