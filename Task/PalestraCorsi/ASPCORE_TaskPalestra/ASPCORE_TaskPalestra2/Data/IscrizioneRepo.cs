﻿using ASPCORE_TaskPalestra2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TaskPalestra2.Data
{
    public class IscrizioneRepo : InterfaceRepo<Iscrizione>
    {
        private readonly db_palestra2Context _context;

        public IscrizioneRepo(db_palestra2Context con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            var temp = _context.Iscrizioni.Where(i => i.IscrizioneId == varId).FirstOrDefault();
            _context.Remove(temp);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<Iscrizione> GetAll()
        {
            //List<Iscrizione> iscrizioni = _context.Iscrizioni.ToList();

            //iscrizioni.ForEach(i =>
            //{
            //    i.CorsoRifNavigation = _context.Corsi.FirstOrDefault(c => c.CorsoId == i.CorsoRif);
            //    i.UtenteRifNavigation = _context.Utenti.FirstOrDefault(u => u.UtenteId == i.UtenteRif);
            //});

            //return iscrizioni;

            return _context.Iscrizioni.ToList();
        }

        public Iscrizione GetByEmail(string varEmail)
        {
            throw new NotImplementedException();
        }

        public Iscrizione GetById(int varId)
        {
            return _context.Iscrizioni.Where(o => o.IscrizioneId == varId).FirstOrDefault();
        }

        public Iscrizione Insert(Iscrizione t)
        {
            Iscrizione temp = _context.Iscrizioni.FirstOrDefault(i => i.UtenteRif == t.UtenteRif && i.CorsoRif == t.CorsoRif);

            if (temp == null && t != null)
            {
                _context.Iscrizioni.Add(t);
                _context.SaveChanges();

                return t;
            }
            return null;
        }

        public bool Update(Iscrizione t)
        {
            throw new NotImplementedException();
        }
    }
}
