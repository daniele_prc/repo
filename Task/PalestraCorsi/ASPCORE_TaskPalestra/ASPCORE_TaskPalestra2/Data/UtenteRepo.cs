﻿using ASPCORE_TaskPalestra2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TaskPalestra2.Data
{
    public class UtenteRepo : InterfaceRepo<Utente>
    {
        private readonly db_palestra2Context _context;

        public UtenteRepo(db_palestra2Context con)
        {
            _context = con;
        }

        public bool Delete(int varId)
        {
            Utente temp = GetById(varId);

            if (temp != null)
            {
                _context.Utenti.Remove(temp);
                _context.SaveChanges();

                return true;
            }
            else
                return false;
        }

        public IEnumerable<Utente> GetAll()
        {
            return _context.Utenti.ToList();
        }

        public Utente GetByEmail(string varEmail)
        {
            return _context.Utenti.Where(u => u.Email == varEmail).FirstOrDefault();
        }

        public Utente GetById(int varId)
        {
            return _context.Utenti.Where(u => u.UtenteId == varId).FirstOrDefault();
        }

        public Utente Insert(Utente t)
        {
            Utente temp = GetByEmail(t.Email);
            if (temp == null)
            {
                _context.Utenti.Add(t);
                _context.SaveChanges();

                return t;
            }
            return null;   
        }

        public bool Update(Utente t)
        {
            _context.Update(t);

            if (_context.SaveChanges() > 0)
                return true;
            
            return false;
        }
    }
}
