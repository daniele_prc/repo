// Realizzare una prima interfaccia nella quale vengono rappresentate il n. di categorie.

// Organizzare i dati del Json in più tabelle:
// 1. Tabella "abbigliamento"
// 2. Tabella "calzature"
// 3. Tabella "borse"

// 4. Tabella "uomo"
// 5. Tabella "donna"

// 6. Tabella "prodotti sotto i 30 euro"
// 7. Tabella "5 prodotti più venduti"

function abbigliamento() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.categoria == "abbigliamento") {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function calzature() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.categoria == "calzature") {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function borse() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.categoria == "borse") {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function uomo() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.genere == "uomo") {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function donna() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.genere == "donna") {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function prezzo() {
    $.getJSON("json/prodotti.json", function(data) {
        var prodotto = '';
        $.each(data, function(key, value) {
            if(value.prezzo < 30) {
                prodotto += '<tr>';
                prodotto += '<td>'+value.categoria+'</td>';
                prodotto += '<td>'+value.genere+'</td>';
                prodotto += '<td>'+value.prodotto+'</td>';
                prodotto += '<td>'+value.prezzo+'</td>';
                prodotto += '<td>'+value.venduti+'</td>';
                prodotto += '</tr>';
            }
        });
        $('#contenuto-tabella').append(prodotto);
     })

     $('#tab1').toggleClass('d-none')
     $('#btn1').toggleClass('d-none')
     $('#btn2').toggleClass('d-none')
     $('#btn3').toggleClass('d-none')
     $('#btn-esc').toggleClass('d-none')
}

function esc() {
    $('#tab1').toggleClass('d-none')
    $('#btn1').toggleClass('d-none')
    $('#btn2').toggleClass('d-none')
    $('#btn3').toggleClass('d-none')
    $('#btn-esc').toggleClass('d-none')
}

// Assente soluzione al problema della duplicazione
// Assente tabella dei 5 prodotti più venduti