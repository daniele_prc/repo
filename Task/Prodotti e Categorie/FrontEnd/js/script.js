// PROBLEMA CORS POLICY
// PROBLEMA STAMPA DA DATABASE


function inserisciProdotto() 
{
    let varNome = $("#inNome").val();
    let varDesc = $("#inDesc").val();
    let varPrezzo = $("#inPrezzo").val();
    let varQuantita = $("#inQuant").val();
    let varCategoria= $("#inCat").val();

    if(varNome == "" || varDesc == "" || varPrezzo == 0 || varQuantita == 0 || varCategoria == "") 
    {
        alert("Inserisci tutti i valori per continuare!");
        return;
    }

    let item = {
        nome: varNome,
        desc: varDesc,
        prezzo: parseFloat(varPrezzo),
        quantita: Number(varQuantita),
        categoria: varCategoria
    };

    fetch("https://localhost:44359/api/prodotti/insert", {
        method:"POST",
        data: JSON.stringify(item),
        contentType: "application/json"
    })
        .then(data => {
            $("#inNome").val();
            $("#inDesc").val();
            $("#inPrezzo").val();
            $("#inQuant").val();
            $("#inCat").val();
        })
        .catch(error => alert(error));
}


function stampaProdotti() 
{
    let contenuto = "";

    fetch("https://localhost:44359/api/prodotti", {
        method: "GET"
    })
        .then(response => response.json())
        .then(data => {
            data.forEach(item => {

                contenuto += 
                    `
                        <tr>
                            <td>${item.nome}</td>
                            <td>${item.desc}</td>
                            <td>${item.prezzo}</td>
                            <td>${item.quantita}</td>
                            <td>${item.categoria}</td>
                        </tr>
                    `
            });
            $("#contenuto-tabella").html(contenuto);
        })
}


if(localStorage.getItem("ListaProdotti") == null)
    localStorage.setItem("ListaProdotti", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("ListaProdotti"));

if(document.getElementById("contenuto-tabella") != null)
    stampaProdotti();
