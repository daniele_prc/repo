﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.Models
{
    public class Prodotto
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [BsonElement("codiceSKU"), Required]
        public string CodiceSKU { get; set; }

        [StringLength(100), BsonElement("nome"), Required]
        public string Nome { get; set; }

        [StringLength(150), BsonElement("descrizione"), Required]
        public string Descrizione { get; set; }

        [BsonElement("prezzo"), Required]
        public float Prezzo { get; set; }

        [BsonElement("quantita"), Required]
        public int Quantita { get; set; }

        public ObjectId Category { get; set; }

        [BsonElement("categoria"), Required, BsonIgnore]
        public string Categoria { get; set; }

        [BsonIgnore]
        public Categoria InfoCategoria { get; set; }
    }
}
