﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.Models
{
    public class Categoria
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [StringLength(50), BsonElement("codice"), Required]
        public string Codice { get; set; }

        [StringLength(100), BsonElement("nome"), Required]
        public string Nome { get; set; }

        [StringLength(150), BsonElement("cognome"), Required]
        public string Descrizione { get; set; }

        [StringLength(50), BsonElement("scaffale"), Required]
        public string Scaffale { get; set; }
    }
}