﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_TaskOggettiCategorie.DAL;
using MONGO_TaskOggettiCategorie.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.Controllers
{
    [Route("api/prodotti")]
    [ApiController]
    public class ProdottoController : Controller
    {
        private readonly ProdottoRepo _repository;

        public ProdottoController(IConfiguration configurazione)
        {
            bool isLocale = configurazione.GetValue<bool>("IsLocale");

            string strConnessione = isLocale == true ?
                configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                configurazione.GetValue<string>("MongoSettings:DatabaseRemoto");

            string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");

            _repository = new ProdottoRepo(strConnessione, nomeDatabase);
        }

        [HttpGet]
        public ActionResult StampaProdotti()
        {
            return Ok(_repository.GetAll());
        }

        [HttpGet("{varId}")]
        public ActionResult StampaProdottoPerId(string varId)
        {
            return Ok(_repository.GetById(ObjectId.Parse(varId)));
        }

        [HttpPost("insert")]
        public ActionResult InserisciProdotto(Prodotto prod)
        {
            if (_repository.Insert(prod))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpDelete("{varId}")]
        public ActionResult EliminaProdotto(string varId)
        {
            if (_repository.Delete(ObjectId.Parse(varId)))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpPut("{varId}")]
        public ActionResult ModifyProduct(string varId, Prodotto prod)
        {
            prod.DocumentID = ObjectId.Parse(varId);
            if (_repository.Update(prod))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }
    }
}
