﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_TaskOggettiCategorie.DAL;
using MONGO_TaskOggettiCategorie.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.Controllers
{
    [ApiController]
    [Route("api/categorie")]
    public class CategoriaController : Controller
    {
        private readonly CategoriaRepo _repository;

        public CategoriaController(IConfiguration configurazione)
        {
            bool isLocale = configurazione.GetValue<bool>("IsLocale");

            string strConnessione = isLocale == true ?
                configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                configurazione.GetValue<string>("MongoSettings:DatabaseRemoto");

            string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");

            _repository = new CategoriaRepo(strConnessione, nomeDatabase);
        }

        [HttpGet]
        public ActionResult StampaCategorie()
        {
            return Ok(_repository.GetAll());
        }

        [HttpGet("{varId}")]
        public ActionResult StampaCategoriaPerId(string varId)
        {
            return Ok(_repository.GetById(ObjectId.Parse(varId)));
        }

        [HttpPost("insert")]
        public ActionResult InserisciCategoria(Categoria cat)
        {
            if (_repository.Insert(cat))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpDelete("{varId}")]
        public ActionResult EliminaCategoria(string varId)
        {
            if (_repository.Delete(ObjectId.Parse(varId)))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }

        [HttpPut("{varId}")]
        public ActionResult ModificaCategoria(string varId, Categoria cat)
        {
            cat.DocumentID = ObjectId.Parse(varId);
            if (_repository.Update(cat))
                return Ok(new { Status = "success" });

            return Ok(new { Status = "error" });
        }
    }
}
