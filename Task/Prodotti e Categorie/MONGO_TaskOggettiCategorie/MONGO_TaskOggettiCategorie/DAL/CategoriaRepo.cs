﻿using MONGO_TaskOggettiCategorie.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.DAL
{
    public class CategoriaRepo : IRepo<Categoria>
    {
        private IMongoCollection<Categoria> categorie;

        public CategoriaRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            if(categorie == null)
            {
                categorie = db.GetCollection<Categoria>("Categorie");
            }
        }

        public bool Delete(ObjectId varId)
        {
            Categoria temp = categorie.Find(i => i.DocumentID == varId).FirstOrDefault();
            if (temp != null)
            {
                var result = categorie.DeleteOne(i => i.DocumentID == varId);
                if (result.IsAcknowledged && result.DeletedCount > 0)
                    return true;
            }
            return false;
        }

        public IEnumerable<Categoria> GetAll()
        {
            return categorie.Find(FilterDefinition<Categoria>.Empty).ToList();
        }

        public Categoria GetById(ObjectId varId) 
        {
            return categorie .Find(i => i.DocumentID == varId).FirstOrDefault();
        }

        public bool Insert(Categoria t)
        {
            Categoria temp = categorie.Find(i => i.Codice == t.Codice).FirstOrDefault();
            if (temp == null)
            {
                t.Codice = Guid.NewGuid().ToString();
                categorie.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(Categoria t)
        {
            Categoria temp = categorie.Find(i => i.DocumentID == t.DocumentID).FirstOrDefault();
            if (temp != null)
            {
                temp.Codice = t.Codice ?? temp.Codice;
                temp.Nome = t.Nome ?? temp.Nome;
                temp.Descrizione = t.Descrizione ?? temp.Descrizione;
                temp.Scaffale = t.Scaffale ?? t.Scaffale;

                var result = categorie.ReplaceOne(i => i.DocumentID == t.DocumentID, temp);
                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
            }
            return false;
        }

        public Categoria FindByName(string varNome)
        {
            return categorie.Find(i => i.Nome == varNome).FirstOrDefault();
        }
    }
}
