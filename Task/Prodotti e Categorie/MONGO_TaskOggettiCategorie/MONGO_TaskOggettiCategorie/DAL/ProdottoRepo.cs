﻿using MONGO_TaskOggettiCategorie.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_TaskOggettiCategorie.DAL
{
    public class ProdottoRepo : IRepo<Prodotto>
    {
        private readonly IMongoCollection<Prodotto> prodotti;

        private readonly string strConn;
        private readonly string strDb;

        public ProdottoRepo(string strConnessione, string strDatabase)
        {
            var client = new MongoClient(strConnessione);
            var db = client.GetDatabase(strDatabase);

            prodotti = db.GetCollection<Prodotto>("Prodotti");
            strConn = strConnessione;
            strDb = strDatabase;
        }

        public bool Delete(ObjectId varId)
        {
            Prodotto temp = prodotti.Find(i => i.DocumentID == varId).FirstOrDefault();
            if (temp != null)
            {
                var result = prodotti.DeleteOne(i => i.DocumentID == varId);
                if (result.IsAcknowledged && result.DeletedCount > 0)
                    return true;
            }
            return false;
        }

        public IEnumerable<Prodotto> GetAll()
        {
            List<Prodotto> elenco = prodotti.Find(FilterDefinition<Prodotto>.Empty).ToList();

            CategoriaRepo tempRepo = new CategoriaRepo(strConn, strDb);

            elenco.ForEach(i => i.InfoCategoria = tempRepo.GetById(i.Category));
            return elenco;
        }

        public Prodotto GetById(ObjectId varId)
        {
            Prodotto temp = prodotti.Find(i => i.DocumentID == varId).FirstOrDefault();

            CategoriaRepo tempRepo = new CategoriaRepo(strConn, strDb);

            temp.InfoCategoria = tempRepo.GetById(temp.Category);
            return temp;
        }

        public bool Insert(Prodotto t)
        {
            Prodotto temp = prodotti.Find(d => d.CodiceSKU == t.CodiceSKU).FirstOrDefault();
            if (temp == null)
            {
                CategoriaRepo tempRepo = new CategoriaRepo(strConn, strDb);
                Categoria tempCate = tempRepo.FindByName(t.Categoria);

                if (tempCate != null)
                {
                    t.Category = tempCate.DocumentID;
                    prodotti.InsertOne(t);
                    if (t.DocumentID != null)
                        return true;
                }
            }
            else
            {
                temp.Quantita += t.Quantita;
                var risultato = prodotti.ReplaceOne(i => i.DocumentID == temp.DocumentID, temp);
                if (risultato.IsAcknowledged && risultato.ModifiedCount > 0)
                    return true;
            }
            return false;
        }

        public bool Update(Prodotto t)
        {
            Prodotto temp = prodotti.Find(i => i.DocumentID == t.DocumentID).FirstOrDefault();
            if (temp != null)
            {
                CategoriaRepo tempRepo = new CategoriaRepo(strConn, strDb);

                temp.CodiceSKU = t.CodiceSKU ?? temp.CodiceSKU;
                temp.Nome = t.Nome ?? temp.Nome;
                temp.Descrizione = t.Descrizione ?? temp.Descrizione;
                temp.Prezzo = t.Prezzo > 0 ? t.Prezzo : temp.Prezzo;
                temp.Quantita = t.Quantita > 0 ? t.Quantita : temp.Quantita;

                temp.Category = t.Category.Equals(ObjectId.Empty) ? temp.Category : t.Category;
                temp.Categoria = tempRepo.GetById(temp.Category).Nome;
                temp.InfoCategoria = tempRepo.GetById(temp.Category);

                var result = prodotti.ReplaceOne(i => i.DocumentID == temp.DocumentID, temp);
                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return true;
            }
            return false;
        }
    }
}
