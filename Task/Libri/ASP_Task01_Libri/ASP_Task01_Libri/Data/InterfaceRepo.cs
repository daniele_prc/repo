﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Task01_Libri.Data
{
    interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetByIsbn(int varIsbn);
        bool Insert(T obj);
    }
}
