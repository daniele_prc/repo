﻿using ASP_Task01_Libri.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Task01_Libri.Data
{
    public class SqlLibro : InterfaceRepo<Libro>
    {
        private string stringConnection = "Server=DESKTOP-5MTRE45\\SQLEXPRESS;Database=db_libri;User Id=sharpuser;Password=pincopallino;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false";

        // RICERCA DI TUTTI I LIBRI
        public IEnumerable<Libro> GetAll()
        {
            List<Libro> elenco = new List<Libro>();

            using (SqlConnection connessione = new SqlConnection(stringConnection))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT libroId, titolo, descrizione, autore, isbn, quantita FROM Libro";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Libro temp = new Libro()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Titolo = reader[1].ToString(),
                        Descrizione = reader[2].ToString(),
                        Autore = reader[3].ToString(),
                        Isbn = reader[4].ToString(),
                        Quantita = Convert.ToInt32(reader[5])
                    };

                    elenco.Add(temp);
                }
            }
            return elenco;
        }

        // RICERCA LIBRO PER CODICE ISBN
        public Libro GetByIsbn(int varIsbn)
        {
            Libro temp = null;

            using (SqlConnection connessione = new SqlConnection(stringConnection))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT libroId, titolo, descrizione, autore, isbn, quantita FROM Libro WHERE isbn = @varIsbn";
                comm.Parameters.AddWithValue("@varIsbn", varIsbn);

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();

                try
                {
                    reader.Read();

                    temp = new Libro()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Titolo = reader[1].ToString(),
                        Descrizione = reader[2].ToString(),
                        Autore = reader[3].ToString(),
                        Isbn = reader[4].ToString(),
                        Quantita = Convert.ToInt32(reader[5])
                    };
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Errore! ;(");
                }
            }
            return temp;
        }

        // INSERIMENTO LIBRO
        public bool Insert(Libro obj)
        {
            using (SqlConnection connessione = new SqlConnection(stringConnection))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Libro (titolo, descrizione, autore, isbn, quantita) VALUES (@varTitolo, @varDescrizione, @varAutore, @varIsbn, @varQuantita)";
                comm.Parameters.AddWithValue("@varTitolo", obj.Titolo);
                comm.Parameters.AddWithValue("@varDescrizione", obj.Descrizione);
                comm.Parameters.AddWithValue("@varAutore", obj.Autore);
                comm.Parameters.AddWithValue("@varAutore", obj.Isbn);
                comm.Parameters.AddWithValue("@varAutore", obj.Quantita);


                connessione.Open();
                try
                {
                    int affRows = comm.ExecuteNonQuery();
                    if (affRows > 0)
                        return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return false;
        }
    }
}
