﻿using ASP_Task01_Libri.Data;
using ASP_Task01_Libri.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Task01_Libri.Controllers
{
    [Route("api/libri")]
    [ApiController]
    public class LibroController : Controller
    {
        private readonly SqlLibro _repository = new SqlLibro();

        [HttpGet]
        public ActionResult<IEnumerable<Libro>> GetAllBooks()
        {
            var elenco = _repository.GetAll();
            return Ok(elenco);
        }

        [HttpGet("{isbn}")]
        public ActionResult<Libro> GetBook(int isbn)
        {
            var libro = _repository.GetByIsbn(isbn);
            return Ok(libro);
        }

        [HttpPost("insert")]
        public ActionResult InsertBook(Libro objLibro)
        {
            if (_repository.Insert(objLibro))
                return Ok("Successo!");
            else
                return Ok("Errore! ;(");
        }
    }
}
