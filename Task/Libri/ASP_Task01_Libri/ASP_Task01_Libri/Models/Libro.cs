﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Task01_Libri.Models
{
    // Creare una REST API per la gestione di libri:

    // Ogni libro è caratterizzato da:
    // Titolo
    // Descrizione
    // Autore
    // ISBN (unico)
    // Quantità

    // Creare i seguenti EndPoint:
    // http://localhost:xxxx/api/libri/inserisci	POST		Inserisce un libro
    // http://localhost:xxxx/api/libri			    GET		    Restituisce tutti i libri
    // http://localhost:xxxx/api/libri/{isbn}		GET		    Restituisce un libro per ISBN

    public class Libro
    {
        public int Id { get; set; }
        public string Titolo { get; set; }
        public string Descrizione { get; set; }
        public string Autore { get; set; }
        public string Isbn { get; set; }
        public int Quantita { get; set; }
    }
}
