// Creare un'applicazione multipagina che sia in grado di gestire un elenco di libri.
// Ogni libro sarà caratterizzato da:
// - Titolo
// - Autore
// - Codice ISBN
// - Prezzo

// Predisporre una pagina dedicata al solo inserimento
// Predisporre una pagina dedicata all'elencazione dei libri già inseriti, modifica ed eliminazione.

// HARD: Sulla tabella predisporre un campo di ricerca con un pulsante che come CTA filtri i risultati all'interno della tabella (per Titolo)


// INSERIMENTO DATI IN TABELLA
function inserimento() {
    let var_titolo = document.getElementById("input_titolo").value;
    let var_autore = document.getElementById("input_autore").value;
    let var_codice = document.getElementById("input_codice").value;
    let var_prezzo = document.getElementById("input_prezzo").value;
    let var_quan = document.getElementById("input_quan").value;

    let libro = {
        id: calcolaId(),
        titolo: var_titolo,
        autore: var_autore,
        codice: var_codice,
        prezzo: parseFloat(var_prezzo),
        quan: parseInt(var_quan),
    }

    if(!isPresente(libro)) {
        elenco.push(libro);
        localStorage.setItem("ListaLibri", JSON.stringify(elenco));

        stampaLibri();
    }
}

function calcolaId(){
    if(elenco.length != 0)
        return elenco[elenco.length - 1].id + 1;

    return 1;
}

function isPresente(objLibro) {
    for(let [index, item] of elenco.entries()) {
        if(objLibro.titolo == item.titolo && objLibro.autore == item.autore) {
            item.quan += objLibro.quan;
            return true;
        }
    }
    return false;
}

function eliminazione(varId) {
    for(let [index, item] of elenco.entries()) {
        if(item.id == varId) {
            elenco.splice(index, 1);
        }
    }

    stampaLibri();
}

// APERTURA DEL MODAL
function aperturaModale(varId) {
    for(let [index, item] of elenco.entries()) {
        if(item.id == varId) {
            document.getElementById("update_id").value = item.id;
            document.getElementById("update_titolo").value = item.titolo;
            document.getElementById("update_autore").value = item.autore;
            document.getElementById("update_codice").value = item.autore;
            document.getElementById("update_prezzo").value = item.prezzo;
            document.getElementById("update_quan").value = item.quan;
        }
    }

    $("#modaleModifica").modal('show');
}

// AGGIORNAMENTO DATI DAL MODAL
function aggiornamento() {
    let var_id = document.getElementById("update_id").value;
    let var_titolo = document.getElementById("update_titolo").value;
    let var_autore = document.getElementById("update_autore").value;
    let var_codice = document.getElementById("update_codice").value;
    let var_prezzo = document.getElementById("update_prezzo").value;
    let var_quan = document.getElementById("update_quan").value;

    let libro = {
        id: var_id,
        titolo: var_titolo,
        autore: var_autore,
        codice: var_codice,
        prezzo: var_prezzo,
        quan: var_quan
    }

    for(let [index, item] of elenco.entries()) {
        if(item.id == libro.id) {
            item.titolo = libro.titolo;
            item.autore = libro.autore;
            item.codice = libro.codice;
            item.prezzo = libro.prezzo;
            item.quan = libro.quan;
        }
    }

    $("#modaleModifica").modal('hide');

    stampaLibri();
}

// RICERCA ELEMENTO IN TABELLA
function ricerca() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("barra-ricerca");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabella");
    tr = table.getElementsByTagName("tr");
  
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[0];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }

// VISUALIZZAZIONE DATI IN TABELLA
function stampaLibri() {
    let contenuto = "";

    for(let[index, item] of elenco.entries()) {
        contenuto += `
            <tr>
                <td>${item.titolo}</td>
                <td>${item.autore}</td>
                <td>${item.codice}</td>
                <td>${item.prezzo}</td>
                <td>${item.quan}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="eliminazione(${item.id})">
                        <i class="fa-solid fa-trash"></i>
                    </button>

                    <button type="button" class="btn btn-outline-warning" onclick="aperturaModale(${item.id})">
                        <i class="fa-solid fa-pencil"></i>
                    </button>
                </td>
            </tr>
        `;
    }

    document.getElementById("contenuto-tabella").innerHTML = contenuto;

}

// ASSENTE LA SOMMA PREZZO TOTALE


if(localStorage.getItem("ListaLibri") == null)
    localStorage.setItem("ListaLibri", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("ListaLibri"));

if(document.getElementById("contenuto-tabella") != null)
    stampaLibri();