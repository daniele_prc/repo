// Realizzare interfaccia composta da due tab; l'utente può effettuare switch tra di esse,
// procedendo all'inserimento dei dati (NomeCanzone, NomeCantante, AnnoPubblicazione) nella 1^ tab,
// alla consultazione dei dati nella 2^. In quest'ultima è possibile eliminare i record in modo individuale, riga per riga.
// Nella 1^ tab vi sono 3 pulsanti, il primo relativo all'inserimento, il secondo al cambio di visualizzazione per la consultazione dei dati,
// il terzo per l'eliminazione di tutti i dati dalla tabella

function scambio() {
    $('#tab1').toggleClass('hide');
    $('#tab2').toggleClass('hide');
}

function inserimento() {
    let var_canzone = document.getElementById("in_canzone").value;
    let var_cantante = document.getElementById("in_cantante").value;
    let var_pubblicazione = document.getElementById("in_pubblicazione").value;

    let elemento = {
        id: calcolaId(),
        canzone: var_canzone,
        cantante: var_cantante,
        anno: var_pubblicazione,
    }

    if(!isPresente(elemento)) {
        elenco.push(elemento);
        localStorage.setItem("ListaCanzoni", JSON.stringify(elenco));

        stampaTabella();
    }
}

function calcolaId(){
    if(elenco.length != 0)
        return elenco[elenco.length - 1].id + 1;

    return 1;
}

function isPresente(objElemento) {
    for(let [index, item] of elenco.entries()) {
        if(objElemento.canzone == item.canzone && objElemento.cantante == item.cantante) {
            return true;
        }
    }
    return false;
}

function eliminazione(varId) {
    for(let [index, item] of elenco.entries()) {
        if(item.id == varId) {
            elenco.splice(index, 1);
        }
    }

    stampaTabella();
}

function aperturaModale(varId) {
    for(let [index, item] of elenco.entries()) {
        if(item.id == varId) {
            document.getElementById("update_id").value = item.id;
            document.getElementById("update_canzone").value = item.canzone;
            document.getElementById("update_cantante").value = item.cantante;
            document.getElementById("update_pubblicazione").value = item.anno;
        }
    }

    $("#modaleModifica").modal('show');
}

function aggiornamento() {
    let var_id = document.getElementById("update_id").value;
    let var_canzone = document.getElementById("update_canzone").value;
    let var_cantante = document.getElementById("update_cantante").value;
    let var_anno = document.getElementById("update_pubblicazione").value;
   
    let libro = {
        id: var_id,
        canzone: var_canzone,
        cantante: var_cantante,
        anno: var_anno,
    }

    for(let [index, item] of elenco.entries()) {
        if(item.id == elemento.id) {
            item.canzone = elemento.canzone;
            item.cantante = elemento.cantante;
            item.anno = elemento.anno;
        }
    }

    $("#modaleModifica").modal('hide');

    stampaTabella();
}

function eliminaTutto() {

    localStorage.clear();
    stampaTabella();
}

function stampaTabella() {
    let contenuto = "";

    for(let[index, item] of elenco.entries()) {
        contenuto += `
            <tr>
                <td>${item.canzone}</td>
                <td>${item.cantante}</td>
                <td>${item.anno}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="eliminazione(${item.id})">
                        <i class="fa-solid fa-trash"></i>
                    </button>

                    <button type="button" class="btn btn-outline-warning" onclick="aperturaModale(${item.id})">
                        <i class="fa-solid fa-pencil"></i>
                    </button>
                </td>
            </tr>
        `;
    }

    document.getElementById("contenuto-tabella").innerHTML = contenuto;

}

if(localStorage.getItem("ListaCanzoni") == null)
    localStorage.setItem("ListaCanzoni", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("ListaCanzoni"));

if(document.getElementById("contenuto-tabella") != null)
    stampaTabella();