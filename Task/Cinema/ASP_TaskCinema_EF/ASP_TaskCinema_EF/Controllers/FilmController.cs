﻿using ASP_TaskCinema_EF.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_TaskCinema_EF.Controllers
{
    [Route("api/film")]
    [ApiController]
    public class FilmController : Controller
    {
        private readonly db_filmContext _context = new db_filmContext();

        [HttpGet]
        public ActionResult<IEnumerable<Film>> GetAllFilm()
        {
            var elenco = _context.Films.ToList();
            return Ok(elenco);
        }

        [HttpGet("{id}")]
        public ActionResult<Film> GetFilmById(int id)
        {
            return Ok(_context.Films.SingleOrDefault(i => i.FilmId == id));
        }

        [HttpPost("insert")]
        public ActionResult InsertFilm(Film objFilm)
        {
            _context.Films.Add(objFilm);

            if (_context.SaveChanges() > 0)
                return Ok("Successo!");
            else
                return Ok("Errore nell'inserimento! ;(");
        }

        [HttpDelete("{id}")]
        public ActionResult<Film> DeleteFilm(int id)
        {
            var temp = _context.Films.SingleOrDefault(i => i.FilmId == id);
            _context.Films.Remove(temp);

            if (_context.SaveChanges() > 0)
                return Ok("Successo!");
            else
                return Ok("Errore nella eliminazione! ;(");
        }

        [HttpPut("{id}")]
        public ActionResult UpdateFilm(Film objFilm, int id)
        {
            var temp = _context.Films.SingleOrDefault(i => i.FilmId == id);
            temp.Titolo = objFilm.Titolo;
            temp.Immagine = objFilm.Immagine;
            temp.Descrizione = objFilm.Descrizione;
            temp.Regista = objFilm.Regista;
            temp.Produttore = objFilm.Produttore;
            temp.Anno = objFilm.Anno;
            temp.Durata = objFilm.Durata;
            temp.Incasso = objFilm.Incasso;

            if (_context.SaveChanges() > 0)
                return Ok("Successo!");
            else
                return Ok("Errore nella modifica! ;(");
        }
    }
}
