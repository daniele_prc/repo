function salvaMessaggi(){
    let msgText = $("#input_messaggio").val();
    if(msgText.length != 0) {
        let utente = {
                nickname: nick,
                contenuto: $("#input_messaggio").val()
        }
    
        $.ajax(
            {
                url: "https://localhost:44367/api/messaggi/insert",
                method: "POST",
                data: JSON.stringify(utente),
                // dataType: "json",
                contentType: "application/json",
                success: function(risposta){
                    console.log(risposta)

                },
                error: function(errore){
                    console.log(errore)
                    alert("Errore")
                },
                complete: function() {
                    $("#input_messaggio").val("")
                }
            }
        );
    }
}

function stampaMessaggi(){
    $.ajax(
        {
            url: "https://localhost:44367/api/messaggi",
            method: "GET",
            success: function(response){

                chat = response;
                let contenuto = "";
                for(let item of chat){
                    contenuto += 
                    `
                        <li class="list-group-item">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-2"><i class="fa-solid fa-user"></i> ${item.nickname}</h4>
                                <small>${item.orario.slice(-7)}</small>
                            </div>
                            <p>${item.contenuto}</p>
                        </li>
                    `;
                }

                $("#contenuto-chat").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function salvaNickname() {
    let varNick = $("#input_nickname").val();
    if(varNick.length != 0) {
        nick = varNick;
        localStorage.setItem("ListaUtenti", JSON.stringify(nick))
        window.location.href = "chat.html";
    }
}

function esci() {
    window.location.href = "index.html"
}

$(document).ready(function() {
    $("#benvenuto").html('<h2 class="text-center">Benvenuto!</h2>'); //ASSENTE BENVENUTO CON NICKNAME DI INPUT
    interval = setInterval(() => stampaMessaggi(), 500);
});


if(localStorage.getItem("ListaUtenti") == null)
    localStorage.setItem("ListaUtenti", JSON.stringify(""))

let nick = '';
nick = JSON.parse(localStorage.getItem("ListaUtenti"));

if(document.getElementById("contenuto-chat") != null)
    stampaMessaggi();