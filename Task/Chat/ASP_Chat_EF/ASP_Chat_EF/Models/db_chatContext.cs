﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ASP_Chat_EF.Models
{
    public partial class db_chatContext : DbContext
    {
        public db_chatContext()
        {
        }

        public db_chatContext(DbContextOptions<db_chatContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Messaggio> Messaggios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-5MTRE45\\SQLEXPRESS;Database=db_chat;User Id=sharpuser;Password=pincopallino;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Messaggio>(entity =>
            {
                entity.ToTable("Messaggio");

                entity.Property(e => e.MessaggioId).HasColumnName("messaggioID");

                entity.Property(e => e.Contenuto)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("contenuto");

                entity.Property(e => e.Nickname)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nickname");

                entity.Property(e => e.Orario)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("orario")
                    .HasDefaultValueSql("(getdate())");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
