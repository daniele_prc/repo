﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ASP_Chat_EF.Models
{
    public partial class Messaggio
    {
        public int MessaggioId { get; set; }
        public string Nickname { get; set; }
        public string Orario { get; set; }
        public string Contenuto { get; set; }
    }
}
