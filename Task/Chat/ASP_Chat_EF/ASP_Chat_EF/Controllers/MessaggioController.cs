﻿using ASP_Chat_EF.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Chat_EF.Controllers
{
    [Route("api/messaggi")]
    [ApiController]
    public class MessaggioController : Controller
    {
        private readonly db_chatContext _context = new db_chatContext();

        [HttpGet]
        public ActionResult<IEnumerable<Messaggio>> GetAllMessages()
        {
            var elenco = _context.Messaggios.ToList();
            return Ok(elenco);
        }

        [HttpPost("insert")]
        public ActionResult InsertMessage(Messaggio objMessaggio)
        {
            _context.Messaggios.Add(objMessaggio);
            if (_context.SaveChanges() > 0)
                return Ok("Successo!");
            else
                return Ok("Errore! ;(");
        }
    }
}
